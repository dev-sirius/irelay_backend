from django.urls import path
from .views import *
urlpatterns = [
    
    # Add admin
    path('createAdmin', CreateAdminView.as_view()),
    
    # Course Related Apis
    path('listCreateCourse', ListCreateCourseView.as_view()),
    path('deleteCourse', DeleteCourseView.as_view()),
    path('retrieveUpdateCourse', RetrieveUpdateCourseView.as_view()),
    
    path('raceLogin', RaceLoginView.as_view()),
    path('getRaceStatus', ListTeamStatusView.as_view()),
    path('updateTeamStartTime', UpdateTeamStartTimeView.as_view()),
    path('updateSubmission', UpdateSubmissionView.as_view()),
    
    # Event Related Apis
    path('listCreateEvent', ListCreateEventView.as_view()),
    path('retrieveUpdateDestoryEvent', RetrieveUpdateDestroyEventView.as_view()),
    
    # Badge Related Apis
    path('listCreateBadge', ListCreateBadgeView.as_view()),
    
    # Team Related Api
    path('listCreateTeam', ListCreateTeamView.as_view()),
    path('retrieveUpdateDestroyTeam', RetrieveUpdateDestroyTeamView.as_view()),
    
    # Race Control Api
    path('startRace', CreateRelayStartView.as_view()),
    path('stopRace', DestroyRelayStopView.as_view()),
    path('track', ListCreateTrackView.as_view()),
    path('handOff', UpdateHandOffView.as_view()),
    
    # Runner Related Api
    path('updateDeviceToken', UpdateDeviceTokenView.as_view()),
    path('listCreateRunner', ListCreateRunnerView.as_view()),
    path('retrieveUpdateDestroyRunner', RetrieveUpdateDestroyRunnerView.as_view()),
    
    # Media Related Api
    path('listCreateMedia', ListCreateMediaView.as_view()),
    
    # Message Related Api
    path('listCreateMessage', ListCreateMessageView.as_view()),
    
    # Setting Related Api
    path('retrieveUpdateSetting', RetrieveUpdateSettingView.as_view()),
    
    # Leg Related Api
    path('listFinishedLegs', ListFinishedLegsView.as_view()),
    path('listFutureLegs', ListFutureSubmissionsView.as_view()),
    
    # Get Event Result
    path('result', ListResultView.as_view()),
    path('listSplitOverview', ListTeamSplitOverviewView.as_view()),
    path('listTeamSplitDetailView', ListTeamSplitDetailView.as_view()),
    
    path('distance', CalculateDistance.as_view()),
    path('publish', ListPublishView.as_view()),
    path('email', CreateSendemailView.as_view()),
    path('sms', CreateSendSmsView.as_view()),
]
