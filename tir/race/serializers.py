from rest_framework import serializers
from .models import *
from .util import *

class CoordinateSerializer(serializers.ModelSerializer):
    
    latitude = serializers.SerializerMethodField()
    longitude = serializers.SerializerMethodField()
    
    def get_latitude(self, obj):
        return float(obj.Location.split(',')[0])
    
    def get_longitude(self, obj):
        return float(obj.Location.split(',')[1] )
    
    class Meta:
        model = Coordinate
        fields = ('latitude', 'longitude')
        

class LegSerializer(serializers.ModelSerializer):
    
    num = serializers.SerializerMethodField()
    miles = serializers.SerializerMethodField()
    total_miles = serializers.SerializerMethodField()
    coordinates = CoordinateSerializer(many=True)
    
    def get_num(self, obj):
        return obj.Num
    
    def get_miles(self, obj):
        return obj.Miles
    
    def get_total_miles(self, obj):
        return obj.TotalMiles
    
    class Meta:
        model = Leg
        fields = ('num', 'miles', 'total_miles', 'coordinates')
        

class CourseSerializer(serializers.ModelSerializer):
    
    # legs = LegSerializer(many=True)
    
    class Meta:
        model = Course
        fields = ('id', 'name', 'photo', 'total_leg', 'start_coordinate', 'finish_coordinate')


class EventSerializer(serializers.ModelSerializer):
    
    held_ts = serializers.SerializerMethodField()
    course_name = serializers.SerializerMethodField() 
    course_id = serializers.SerializerMethodField()
    team_ids = serializers.SerializerMethodField()
    
    def get_team_ids(self, obj):
        teams = obj.team_set.all()
        ids = []
        if len(teams) == 0:
            return []
        for team in teams:
            ids.append(team.id)
        return ids
    
    def get_course_id(self, obj):
        return obj.course.id
    
    def get_course_name(self, obj):
        return obj.course.name
    
    def get_held_ts(self,obj):
        return get_timestamp(obj.date_held)
    
    class Meta:
        model = Event
        fields = ('id', 'title',  'course_name', 'course_id', 'team_ids', 'held_ts')


class MediaSerializer(serializers.ModelSerializer):

    created_ts = serializers.SerializerMethodField()
    member_name = serializers.SerializerMethodField()

    class Meta:
        model = Media
        fields = ('id', 'media_type', 'member_name', 'thumb', 'content', 'created_ts')

    def get_created_ts(self, medium):
        return get_timestamp(medium.created)

    def get_member_name(self, medium):
        return "%s %s" % (medium.runner.FirstName, medium.runner.LastName)


class MessageSerializer(serializers.ModelSerializer):

    created_ts = serializers.SerializerMethodField()
    sender_name = serializers.SerializerMethodField()

    def get_created_ts(self, obj):
        return get_timestamp(obj.created)

    def get_sender_name(self, obj):
        return "%s %s" % (obj.runner.FirstName, obj.runner.LastName)

    class Meta:
        model = Message
        fields = ('sender_name', 'message', 'created_ts')


class TeamSettingSerializer(serializers.ModelSerializer):
    class Meta:
        model = TeamSetting
        fields = '__all__'


class RunnerNotificationSettingSerializer(serializers.ModelSerializer):
    class Meta:
        model = RunnerNotificationSetting
        fields = "__all__"


class BadgeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Badge
        fields = '__all__'


class TeamSerializer(serializers.ModelSerializer):
    event_id = serializers.SerializerMethodField()
    join_code = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()
    type = serializers.SerializerMethodField()
    classification = serializers.SerializerMethodField()
    photo = serializers.SerializerMethodField()
    city = serializers.SerializerMethodField()
    course_id =serializers.SerializerMethodField()
    runner_count = serializers.SerializerMethodField()
    
    class Meta:
        model = Team
        fields = ('id', 'join_code', 'runner_count', 'event_id', 'course_id', 'name', 'city', 'photo', 'type', 'classification')
    
    def get_course_id(self, obj):
        return obj.Event.course.id
    
    def get_runner_count(self, obj):
        return len(obj.runner_set.all())
    
    def get_city(self, obj):
        return obj.City

    def get_event_id(self, obj):
        return obj.Event.id

    def get_join_code(self, obj):
        return obj.JoinCode

    def get_photo(self, obj):
        return obj.Logo.url

    def get_name(self, obj):
        return obj.Name

    def get_type(self, obj):
        return obj.Type

    def get_classification(self, obj):
        return obj.Classification


class RunnerSerializer(serializers.ModelSerializer):

    name = serializers.SerializerMethodField()
    pace = serializers.SerializerMethodField()
    is_captain = serializers.SerializerMethodField()
    is_roadie = serializers.SerializerMethodField()
    photo = serializers.SerializerMethodField()

    def get_name(self, obj):
        return "%s %s" % (obj.FirstName, obj.LastName)

    def get_pace(self, obj):
        return obj.Pace

    def get_is_captain(self, obj):
        return obj.IsCaptain

    def get_is_roadie(self, obj):
        return obj.IsRoadie

    def get_photo(self, obj):
        return obj.Photo.url

    class Meta:
        model = Runner
        fields = ('id', 'name', 'is_captain', 'photo', 'is_roadie', 'pace')
    