from django.test import TestCase
from .models import *
from .util import *
from .firebase_models import *


# Create your tests here.
class MyTest(TestCase):

    def setUp(self):
        self.start_coordinate = LatLng(latitude=39.091868, longitude=-9.263187)
        self.end_coordinate = LatLng(latitude=39.089815, longitude=-9.261857)
        number_of_points = 20
        self.inner_coordinates = []
        for i in range(0, number_of_points):
            self.inner_coordinates.append(LatLng(
               latitude=(abs(self.start_coordinate.latitude - self.end_coordinate.latitude) / 10) * i + self.end_coordinate.longitude,
               longitude=(abs(self.start_coordinate.longitude - self.end_coordinate.longitude)  / number_of_points) * i + self.end_coordinate.longitude
            ))

    def check_is_between_two_coordinates_function(self):
        for coordinate in self.inner_coordinates:
            self.assertEqual(is_between_two_coordinates(start_coordinate=self.start_coordinate, end_coordinate=self.end_coordinate, target_coordinate=coordinate), True)