from django_cron import CronJobBase, Schedule
from django.conf import settings
import os
import datetime

class MyCronJob(CronJobBase):
    RUN_EVERY_MINS = 1 # every 15 minutes
    
    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'race.relay_job'    # a unique code
    
    def do(self):
        print('This is printed by cron job')
        log_path = os.path.join(settings.BASE_DIR, settings.MEDIA_ROOT, 'log.txt')
        handle=open(log_path,'w+')
        print(datetime.datetime.now().second)
        now = datetime.datetime.now().second
        handle.write("%dh %dm %ds" % (datetime.datetime.now().hour, datetime.datetime.now().minute, datetime.datetime.now().second))