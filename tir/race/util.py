from .models import LatLng
from geographiclib.geodesic import Geodesic
from django.core.mail import send_mail
from .constants import *
from twilio.rest import Client
from django.conf import settings


def send_sms(sender_phone, message):
    client = Client(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)
    response = client.messages.create(
        body=message, 
        to=sender_phone, 
        from_=settings.TWILIO_PHONE_NUMBER
    )


# In miles
def calculate_distance(coordinate1, coordinate2):
    # Define the ellipsoid
    geode = Geodesic.WGS84
    lat1, lon1 = coordinate1
    lat2, lon2 = coordinate2
    g = geode.Inverse(lat1, lon1, lat2, lon2)
    return round((g['s12'] / 1609), 6)


# distance in miles
def find_coordinate_between_tow_coordinates(coordinate1, coordinate2, distance):

    distance *= 1609
    
    # Define the ellipsoid
    geode = Geodesic.WGS84

    # Solve the Inverse problem
    inv = geode.Inverse(coordinate1[0], coordinate1[1], coordinate2[0], coordinate2[1])
    azi1 = inv['azi1']

    # Solve the Direct problem
    direct = geode.Direct(coordinate1[0], coordinate1[1], azi1, distance)
    return [direct['lat2'], direct['lon2']]


def get_or_none(class_model, **kwargs):
    try:
        return class_model.objects.get(**kwargs)
    except class_model.DoesNotExist:
        return None


def build_exchange_notification(leg_num, runner_id, leg_miles):
    return {
        'race_status': {
            'NextLeg': leg_num,
            'NextRunnerId': runner_id,
            'NextLegMiles': float(leg_miles),
        }
    }


# Check Whether target_coordinates lies on between start_coordinate and end_coordinate
def is_between_two_coordinates(start_coordinate: LatLng, end_coordinate: LatLng, target_coordinate: LatLng):
    eps = 0.00001
    if abs(end_coordinate.latitude - start_coordinate.latitude) < eps:
        print()
        if (target_coordinate.longitude <= end_coordinate.longitude) and (target_coordinate.longitude >= start_coordinate.longitude):
            return True
        else:
            return False
    
    else:
        a = (end_coordinate.longitude - start_coordinate.longitude) / (end_coordinate.latitude - start_coordinate.latitude)
        b = (start_coordinate.longitude * end_coordinate.latitude - end_coordinate.longitude * start_coordinate.latitude) / (end_coordinate.latitude - start_coordinate.latitude)

        if abs(target_coordinate.longitude - (a * target_coordinate.latitude + b)) < eps:
            return True
        else:
            return False


def send_email(email_address, message):
    if not TEST:
        send_mail(
            'iRelay',
            message,
            'kingdragon2108@gmail.com',
            [email_address],
            fail_silently=False,
        )


def get_timestamp(date_time):
    return int(date_time.timestamp() * 1000)