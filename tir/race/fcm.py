from fcm_django.fcm import fcm_send_topic_message


# send notification to certain topic
def push_notification(topic, title, description, data):
    fcm_send_topic_message(topic_name=topic, message_body=title, message_title=description, data_message=data)


MEDIA = 'media'
EXCHANGE = 'exchange'
