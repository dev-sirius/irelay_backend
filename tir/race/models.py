from django.db import models
from fcm_django.models import *
from .validators import *


class LatLng:
    def __init__(self, latitude: float, longitude: float):
        self.latitude = latitude
        self.longitude = longitude

    def __str__(self):
        return "latitude: %f, longitude: %f" % (self.latitude, self.longitude)


# Represents Staffs And Volunteers JoinCode and Admin Level
class AppAdmin(models.Model):
    Code = models.CharField(max_length=10)
    AdminLevel = models.IntegerField()
    
    def __str__(self):
        return "%s %d" % (self.Code, self.AdminLevel)


# Represents Virtual Course, This project will have many courses in future, but now there is only one course - TIR Course
class Course(models.Model):
    name = models.CharField(max_length=120)
    total_leg = models.IntegerField(default=0, blank=True)
    start_coordinate = models.CharField(max_length=20, blank=True)
    finish_coordinate = models.CharField(max_length=20, blank=True)
    photo = models.ImageField(upload_to='thumbs')
    file = models.FileField(upload_to='maps')
    

# Represents All Events
class Event(models.Model):
    title = models.CharField(max_length=50)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    date_held = models.DateTimeField()
    nextStatsUpdate = models.DateTimeField(blank=True, null=True)


class Badge(models.Model):
    name = models.CharField(max_length=120)
    description = models.CharField(max_length=120)
    image = models.ImageField(upload_to='badges')
    

# Represents One Leg of Whole Course
class Leg(models.Model):
    Num = models.IntegerField()
    Miles = models.FloatField()
    TotalMiles = models.FloatField()
    Course = models.ForeignKey(Course, related_name='legs', on_delete=models.CASCADE)
    
    def __str__(self):
        return "%d %d" % (self.Id, self.Num)


# Represents Routes For Leg
class Coordinate(models.Model):
    Location = models.CharField(max_length=120)
    Leg = models.ForeignKey(Leg, related_name='coordinates', on_delete=models.CASCADE)
    

class TeamSetting(models.Model):
    
    showTeamPhoto = models.BooleanField(default=True, blank=True)
    showTeamName = models.BooleanField(default=True, blank=True)
    showTeamHomeCity = models.BooleanField(default=True, blank=True)
    showCurrentRunnerFirstName = models.BooleanField(default=True, blank=True)
    showCurrentRunnerLastName = models.BooleanField(default=True, blank=True)
    showCurrentLegNDistance = models.BooleanField(default=True, blank=True)
    showOverallTeamPace = models.BooleanField(default=True, blank=True)
    allowRealCoursesViewable = models.BooleanField(default=True, blank=True)
    exchangeNotifications = models.IntegerField(default=11, blank=True)
    
    showRunnerPhoto = models.BooleanField(default=True, blank=True)
    showRunnerName = models.BooleanField(default=True, blank=True)
    showRunnerStartTime = models.BooleanField(default=True, blank=True)
    showRunnerElapsedTime = models.BooleanField(default=True, blank=True)
    showRunnerCurrentDistance = models.BooleanField(default=True, blank=True)
    showRunnerPace = models.BooleanField(default=True, blank=True)


# Represents All Teams
class Team(models.Model):
    Event = models.ForeignKey(Event, on_delete=models.CASCADE)
    # Bib = models.IntegerField()
    # DiscountCode = models.CharField(max_length=120)
    # IsSplitPayment = models.IntegerField()
    # IsPaid = models.IntegerField()
    # PaidAmount = models.IntegerField()
    # CaptainID = models.IntegerField()
    # ExpectedPace = models.IntegerField()
    JoinCode = models.CharField(max_length=10)
    Name = models.CharField(max_length=120)
    #Captain = models.CharField(max_length=120)
    # Song = models.CharField(max_length=120)
    City = models.CharField(max_length=20)
    Type = models.CharField(max_length=20)
    Classification = models.CharField(max_length=20)
    # IsUntimed = models.IntegerField()
    Logo = models.ImageField(upload_to='team_logos', default='team_default.png')
    StartTime = models.CharField(max_length=30, blank=True, null=True)
    Setting = models.ForeignKey(TeamSetting, on_delete=models.CASCADE)
    IsLocked = models.BooleanField(default=False, blank=True)
    Badges = models.ManyToManyField(Badge)


class RunnerNotificationSetting(models.Model):
    whenKills = models.BooleanField(default=True, blank=True)
    whenGettingKilled = models.BooleanField(default=True, blank=True)
    whenTeamMemberKills = models.BooleanField(default=True, blank=True)
    whenTeamMemberGettingKilled = models.BooleanField(default=True, blank=True)
    whenContributionToTeamPage = models.BooleanField(default=True, blank=True)
    whenISpeedUp = models.BooleanField(default=True, blank=True)
    atEachMileSplitOfMyLegs = models.BooleanField(default=True, blank=True)
    whenEarningBadge = models.BooleanField(default=True, blank=True)


# Represent All Runners In Database
class Runner(models.Model):
    Team = models.ForeignKey(Team, on_delete=models.CASCADE)
    IsCaptain = models.BooleanField()
    FirstName = models.CharField(max_length=20)
    LastName = models.CharField(max_length=20)
    Email = models.EmailField(blank=True)
    State = models.CharField(max_length=10, blank=True)
    Phone = models.CharField(max_length=20, blank=True)
    # Street = models.CharField(max_length=120, blank=True)
    City = models.CharField(max_length=120, blank=True)
    Zip = models.CharField(max_length=120, blank=True)
    DOB = models.DateField(blank=True)
    Pace = models.IntegerField()
    Gender = models.CharField(max_length=1, blank=True)
    ShirtSize = models.CharField(max_length=120, blank=True)
    SockSize = models.CharField(max_length=120, blank=True, null=True)
    # PassHash = models.TextField(blank=True)
    # IsPaid = models.IntegerField(blank=True, default=0)
    # PaidAmount = models.IntegerField(blank=True, default=0)
    # RulesAgreedCST = models.DateTimeField(blank=True)
    # IsDeleted = models.IntegerField(blank=True, default=0)
    # ReferredByEntity = models.CharField(max_length=120, blank=True)
    # ReferredByPerson = models.CharField(max_length=120, blank=True)
    # PushToken = models.TextField(blank=True, null=True)
    IsRoadie = models.BooleanField(blank=True, default=0)
    FCMDevice = models.ForeignKey(FCMDevice, on_delete=models.CASCADE, blank=True, null=True)
    Photo = models.ImageField(upload_to='profile_images', default='runner_default.png')
    RunnerSetting = models.ForeignKey(RunnerNotificationSetting, on_delete=models.CASCADE)
    Badges = models.ManyToManyField(Badge)


# Leg Vs Runner Submission Table 
class Submission(models.Model):
    # RecorderId = models.IntegerField(blank=True, default=0)
    Leg = models.ForeignKey(Leg, on_delete=models.CASCADE)
    # IsAdmin = models.IntegerField(blank=True, default=0)
    Team = models.ForeignKey(Team, on_delete=models.CASCADE)
    ReceivedDt = models.DateTimeField(blank=True, auto_now_add=True)
    ExchangeDt = models.DateTimeField(blank=True, auto_now_add=True)
    Runner = models.ForeignKey(Runner, on_delete=models.CASCADE)
    IsFinished = models.BooleanField(blank=True, default=0)
    Pace = models.IntegerField(default=540)
    
    def __str__(self):
        return "%d" % self.Leg.Num


# Represents Real Running Track Of Leg
class LegTrack(models.Model):
    Submission = models.ForeignKey(Submission, on_delete=models.CASCADE)
    TrackDt = models.DateTimeField(auto_now_add=True)
    Location = models.CharField(max_length=255)
    Distance = models.FloatField()
    Speed = models.FloatField()

    def __str__(self):
        return self.Location


class Media(models.Model):
    MediaType = (
        ('Image', "image file"),
        ('Video', "video file"),
    )
    media_type = models.CharField(choices=MediaType, max_length=5, default='Image')
    thumb = models.ImageField(upload_to='thumbs', blank=True, null=True)
    content = models.FileField(upload_to='team_media')
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    runner = models.ForeignKey(Runner, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)


class Message(models.Model):
    message = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    runner = models.ForeignKey(Runner, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)


# Represents Team Current Race Status
class TeamStats(models.Model):
    Team = models.ForeignKey(Team, on_delete=models.CASCADE)
    Submission = models.ForeignKey(Submission, on_delete=models.DO_NOTHING)
    TeamStartDt = models.DateTimeField()
    CurrentStartDt = models.DateTimeField()
    TotalMile = models.FloatField(default=0, blank=True)
    # Previously Passed Coordinate on Current Leg
    Coordinate = models.ForeignKey(Coordinate, on_delete=models.DO_NOTHING, blank=True, null=True)
    # Distance From Leg Start Coordinate To Previously Passed Coordinate
    DistanceOnVirtualCourse = models.FloatField(default=0, blank=True)
    # Virtual Runner Coordinate On Current Leg
    OverallPace = models.FloatField(default=0, blank=True)
    LocationOnVirtualCourse = models.CharField(max_length=120, blank=True, null=True)
    IsProcessing = models.BooleanField(default=False)
    IsFinished = models.BooleanField(default=False)
    UpdatedDt = models.DateTimeField(auto_now_add=True, blank=True)


class AdminSetting(models.Model):
    register_email_text = models.TextField()
    allow_staffs = models.BooleanField(default=True, blank=True)
    