from random import randint
import json
from rest_framework.parsers import JSONParser
from .firestore import *
from .serializers import *
from pykml import parser
from django.core.files.storage import default_storage
from django.http import JsonResponse
from .constants import ALLOW_PUSH_NOTIFICATION
from .fcm import *
from .models import *
from rest_framework import permissions
from rest_framework import generics
from rest_framework.views import status
from rest_framework.response import Response
import datetime
from django.conf import settings
from .util import *
from .pub import *
from django.db import transaction

def award_badge(class_model, badge_name):
    try:
        badge = class_model.Badges.get(name=badge_name)
    except:
        badge = Badge.objects.get(name=badge_name)
        class_model.Badges.add(badge)


def get_error_response(message, server_status):
    return Response(
        data={
            'isSuccess': False,
            'error': message
        },
        status=server_status
    )


def get_success_response(data):
    return Response(
        data={
            'isSuccess': True,
            'error': '',
            'data': data
        },
        status=status.HTTP_200_OK
    )


def assign_runner_legs(team_id):
    # If Race Has Been Started Already, Then Not Rearrange Submissions
    team = get_or_none(Team, id=team_id)
    team_stats = get_or_none(TeamStats, Team=team)
    if team_stats is not None:
        return
    team.submission_set.all().delete()
    runners = team.runner_set.filter(IsRoadie=0)
    runner_idx = 0
    submissions = []
    
    total_leg = team.Event.course.total_leg
    
    for legNum in range(1, total_leg + 1, 1):
        leg = Leg.objects.get(Num=legNum, Course=team.Event.course)
        runner = runners[runner_idx % len(runners)]
        submissions.append(Submission(
            Team=team,
            Leg=leg,
            Runner=runner,
            Pace=runner.Pace
        ))
        runner_idx += 1
    Submission.objects.bulk_create(submissions)


def get_team_race_detail(team_id):
    # Check Parameter Existence
    if team_id is None:
        return 'TeamId is required'

    # Find Team
    team = get_or_none(Team, id=team_id)
    if team is None:
        return 'Team not found'
    
    current_leg = 0
    current_leg_miles = 0
    current_runner = None
    current_runner_virtual_coordinate = None
    previous_coordinate = None
    
    # Get Submission that are currently running
    team_stats = get_or_none(TeamStats, Team=team)
    
    if team_stats is None:
        # Read From TeamStartTime
        if team.StartTime is None:
            team_start_dt = "2020-03-02 7:50:00"
            current_start_dt = team_start_dt
        else:
            team_start_dt = team.StartTime
            current_start_dt = team.StartTime
    else:        
        current_leg = team_stats.Submission.Leg.Num

        current_runner_virtual_coordinate = team_stats.LocationOnVirtualCourse
        previous_coordinate = team_stats.Coordinate.Location
        
        if current_leg == 36 and team_stats.IsFinished:
            current_leg = 37
        if current_leg == 37:
            current_leg_miles = 0.0
        else:
            current_leg_miles = team_stats.Submission.Leg.Miles
            current_runner = team_stats.Submission.Runner
        team_start_dt = team_stats.TeamStartDt
        current_start_dt = team_stats.CurrentStartDt
    
    current_leg_tracks = []
    if current_leg is not 0 and current_leg is not 37:
        completed_submissions = team.submission_set.get(Leg=Leg.objects.get(Num=current_leg, Course=team.Event.course))
        leg_tracks = completed_submissions.legtrack_set.all()
        for track in leg_tracks:
            current_leg_tracks.append({
                'Location': track.Location,
                'Distance': float(track.Distance),
                'Speed': float(track.Speed)
            })
    
    if type(team_start_dt) != str:
        team_start_dt = int(team_start_dt.timestamp() * 1000)
        current_start_dt = int(current_start_dt.timestamp() * 1000)
    
    sync_data = {
        'TeamStartDateTime': team_start_dt,
        'CurrentStartDateTime': current_start_dt,
        'CurrentLeg': current_leg,
        'CurrentLegMiles': float(current_leg_miles),
        'RunnerLocation': current_runner_virtual_coordinate,
        'RunnerPrevLocation': previous_coordinate,
        'CurrentTracks': current_leg_tracks,
    }
    
    # Get All Team Race Overview
    event = team.Event
    teams = event.team_set.all()
    
    all_team_stats = []
    
    for team in teams:
        team_stats = None
        prev_coordinate = None
        current_location = None
        leg_num = None
        leg_miles = None
        runner = None
        overall_pace = None
        try:
            team_stats = team.teamstats_set.all()[0]
            prev_coordinate = team_stats.Coordinate.Location
            current_location = team_stats.LocationOnVirtualCourse
            leg_num = team_stats.Submission.Leg.Num
            leg_miles = team_stats.Submission.Leg.Miles
            overall_pace = team_stats.OverallPace
            runner = RunnerSerializer(team_stats.Submission.Runner).data
        except:
            pass        
        all_team_stats.append({
            'team_info': TeamSerializer(team).data,
            'current_location': current_location,
            'current_user': runner,
            'prev_coordinate': prev_coordinate,
            'leg_num': leg_num,
            'leg_miles': leg_miles,
            'overall_pace': overall_pace
        })

    sync_data['all_teams'] = all_team_stats
    
    if current_leg == 37:
        race_finish_dt = team_stats.Submission.ExchangeDt
        sync_data['RaceFinishTs'] = get_timestamp(race_finish_dt)
    
    if team_stats is not None:
        sync_data['DistanceOnVirtualCourse'] = team_stats.DistanceOnVirtualCourse
    
    if current_runner is not None:
        sync_data['CurrentRunner'] = RunnerSerializer(current_runner).data
    
    return sync_data


class CreateAdminView(generics.CreateAPIView):
    permission_classes = (permissions.AllowAny,)
    
    def post(self, request, *args, **kwargs):
        code = request.data.get('code')
        admin_level = request.data.get('admin_level')
        admin = AppAdmin.objects.create(
            Code=code,
            AdminLevel=admin_level
        )
        return get_success_response(data={
            'code': code,
            'admin_level': admin_level
        })


class ListCreateCourseView(generics.ListCreateAPIView):
    """
    POST : Upload Course Detail Including Legs And Routes For Legs Using KML File
    """
    permission_classes = (permissions.AllowAny,)
    serializer_class = CourseSerializer
    
    
    def get(self, request, *args, **kwargs):
        courses = Course.objects.all()
        return get_success_response(data=CourseSerializer(courses, many=True).data)
    
    @transaction.atomic
    def post(self, request, *args, **kwargs):
        
        map_file = request.FILES.get('map_file')
        photo_file = request.FILES.get('photo')
        name = request.data.get('name')
        
        if map_file is None:
            return get_error_response(message='Map file is required', server_status=status.HTTP_400_BAD_REQUEST)
        
        if photo_file is None:
            return get_error_response(message='Photo image is required', server_status=status.HTTP_400_BAD_REQUEST)
        
        if name is None:
            return get_error_response(message='Course name is required', server_status=status.HTTP_400_BAD_REQUEST)
        
        course = get_or_none(Course, name=name)
        if course is not None:
            return get_error_response(message='Course already exists', server_status=status.HTTP_400_BAD_REQUEST)
        
        course = Course.objects.create(
            name=name,
            photo=photo_file,
            file=map_file
        )
        
        file_path = course.file.path
        
        start_coordinate = None
        finish_coordinate = None
        leg_list = []
        
        with open(file_path) as f:
            doc = parser.parse(f).getroot()
            course_name = doc.Document.name
            for ch in doc.Document.Folder.getchildren():
                if 'Placemark' in ch.tag:
                    # Found a Place mark
                    leg_name = ch.name.text
                    if leg_name == 'The Start!':
                        start_coordinate = ch.Point.coordinates.text
                        continue
                    
                    if leg_name == 'The Finish!':
                        finish_coordinate = ch.Point.coordinates.text
                        continue
                    
                    if not leg_name.startswith('Leg'):
                        continue
                    try:
                        leg_name = leg_name.lstrip("Leg")
                        leg_num = leg_name.split("-")[0]
                        leg_num = leg_num.replace(" ", "")
                        leg_num = int(leg_num)
                        leg_miles = leg_name.split("-")[1]
                        leg_miles = leg_miles.replace(" ", "")
                        leg_miles = leg_miles.replace('m', "")
                        leg_miles = float(leg_miles)
                    except:
                        continue
                    try:
                        line_string = ch.LineString
                    except:
                        continue
                    coordinates = line_string.coordinates.text
                    leg_list.append({
                        'leg_num': leg_num,
                        'leg_miles': leg_miles,
                        'coordinates': coordinates.split('\n')
                    })
        
        course.total_leg = len(leg_list)
        start_coordinate = start_coordinate.replace(" ", "")
        start_coordinate = start_coordinate.replace("\n", "")
        print('start coordinate = %s' % start_coordinate)
        finish_coordinate = finish_coordinate.replace(" ", "")
        finish_coordinate = finish_coordinate.replace("\n", "")
        print('finish coordinate = %s' % finish_coordinate)
        print("%s,%s" % (start_coordinate.split(",")[1], start_coordinate.split(",")[0]))
        course.start_coordinate = "%s,%s" % (start_coordinate.split(",")[1], start_coordinate.split(",")[0])
        course.finish_coordinate = "%s,%s" % (finish_coordinate.split(",")[1], finish_coordinate.split(",")[0])
        course.save()
        total_miles = 0
        
        legs = []
        for leg_info in leg_list:
            total_miles += leg_info['leg_miles']
            total_miles = round(total_miles, 2)
            legs.append(Leg(Num=leg_info['leg_num'], Miles=leg_info['leg_miles'], TotalMiles=total_miles, Course=course))
        
        Leg.objects.bulk_create(legs)
        
        routes = []
        
        for leg_info in leg_list:
            for coordinate in leg_info['coordinates']:
                coordinate = coordinate.replace(" ", "")
                if len(coordinate.split(",")) == 3:
                    leg = Leg.objects.get(Num=leg_info['leg_num'], Course=course)
                    routes.append(Coordinate(Location="%s,%s" % (coordinate.split(",")[1], coordinate.split(",")[0]), Leg=leg))
        
        Coordinate.objects.bulk_create(routes)
        
        return get_success_response(data=CourseSerializer(course).data)


class DeleteCourseView(generics.DestroyAPIView):
    permission_classes = (permissions.AllowAny,)

    def delete(self, request, *args, **kwargs):
        course_name = request.data.get('CourseName')
        if course_name is None:
            return get_error_response(message='CourseName is required', server_status=status.HTTP_400_BAD_REQUEST)

        course = get_or_none(Course, Name=course_name)
        if course is None:
            return get_error_response(message='Course not found', server_status=status.HTTP_400_BAD_REQUEST)

        course.delete()
        return get_success_response(data={
            'message': 'deleted course'
        })


class ListCreateBadgeView(generics.ListCreateAPIView):
    permission_classes = (permissions.AllowAny,)
    
    def get(self, request, *args, **kwargs):
        runner_id = request.query_params.get('runner_id')
        if runner_id is None:
            return get_error_response(message='Runner id is required', server_status=status.HTTP_400_BAD_REQUEST)
        
        runner = get_or_none(Runner, id=runner_id)
        if runner is None:
            return get_error_response(message='Runner not found', server_status=status.HTTP_400_BAD_REQUEST)
        
        runner_badges = runner.Badges.all()
        team_badges = runner.Team.Badges.all()
        return get_success_response(data=BadgeSerializer(runner_badges, many=True).data + BadgeSerializer(team_badges, many=True).data)


class RetrieveUpdateCourseView(generics.RetrieveUpdateAPIView):
    permission_classes = (permissions.AllowAny,)
    
    def get(self, request, *args, **kwargs):
        course_id = request.query_params.get('course_id')
        if course_id is None:
            return get_error_response(message='Course id is required', server_status=status.HTTP_400_BAD_REQUEST)
        
        course = get_or_none(Course, id=course_id)

        if course is None:
            return get_error_response(message='Course not found', server_status=status.HTTP_400_BAD_REQUEST)
        
        legs = course.legs
        return get_success_response(data={
            'start_coordinate': course.start_coordinate,
            'finish_coordinate': course.finish_coordinate,
            'legs': LegSerializer(legs, many=True).data,
        })


class ListCreateEventView(generics.ListCreateAPIView):
    """
    GET: List Events
    POST: Create Event
    """
    permission_classes = (permissions.AllowAny,)
    serializer_class = EventSerializer
    
    def post(self, request, *args, **kwargs):
        title = request.data.get('title')
        course_id = request.data.get('course_id')
        str_date_held = request.data.get('date_held')

        if course_id is None or str_date_held is None or title is None:
            return get_error_response(message='Some parameters are missing', server_status=status.HTTP_400_BAD_REQUEST)

        course = get_or_none(Course, id=course_id)
        if course is None:
            return get_error_response(message='Course not found', server_status=status.HTTP_400_BAD_REQUEST)

        try:
            new_event = Event.objects.create(
                title=title,
                course=course,
                date_held=datetime.datetime.strptime(str_date_held, '%m/%d/%Y %H:%M:%S'),
            )
            return get_success_response(data=EventSerializer(new_event).data)
        except:
            return get_error_response(message='Failed to create event', server_status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    
    def get(self, request, *args, **kwargs):
        events = Event.objects.filter(date_held__gte=datetime.datetime.today())
        return get_success_response(data=EventSerializer(events, many=True).data)


class RetrieveUpdateDestroyEventView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.AllowAny,)
    """
    DELETE: Delete Course
    """

    def delete(self, request, *args, **kwargs):
        event_id = request.query_params.get('event_id')
        
        if event_id is None:
            return get_error_response(message='Event is is required', server_status=status.HTTP_400_BAD_REQUEST)
        
        event = get_or_none(Event, id=event_id)
        
        if event is None:
            return get_error_response(message='Event does not exist', server_status=status.HTTP_400_BAD_REQUEST)
        
        event.delete()
        return get_success_response(data={
            'event_id': int(event_id)
        })
        
    
class RetrieveUpdateDestroyTeamView(generics.RetrieveUpdateDestroyAPIView):
    """
    GET: Fetch Team
    DELETE: Delete Team
    """
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):
        join_code = request.query_params.get('JoinCode')
        if join_code is None:
            return get_error_response(message='JoinCode is required', server_status=status.HTTP_400_BAD_REQUEST)

        team = get_or_none(Team, JoinCode=join_code)
        if team is None:
            return get_error_response(message='Team not found', server_status=status.HTTP_400_BAD_REQUEST)

        try:
            captain = team.runner_set.get(IsCaptain=1)
            captain_name = "%s %s" % (captain.FirstName, captain.LastName)
        except:
            pass

        res_data = TeamSerializer(team).data
        #print(res_data)
        res_data.update({'captain_name': captain_name})
        # res_data = {'id': 1, 'name': 123, 'captain_name': "123"}
        return get_success_response(data=res_data)

    def delete(self, request, *args, **kwargs):
        join_code = request.query_params.get('join_code')
        if join_code is None:
            return get_error_response(message='Joincode is required', server_status=status.HTTP_400_BAD_REQUEST)
        team = get_or_none(Team, JoinCode=join_code)
        if team is None:
            return get_error_response(message='Team not found', server_status=status.HTTP_400_BAD_REQUEST)

        fb_delete_team_stats(event_id=team.Event.id, join_code=join_code)
        Team.objects.get(JoinCode=join_code).delete()
        return get_success_response(data={
            'join_code': join_code
        })


class ListCreateTeamView(generics.ListCreateAPIView):
    """
    GET: List Team
    POST: Create Team
    """
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):
        event_id = request.query_params.get('event_id')
        if event_id is None:
            return get_error_response(message='Event id is required', server_status=status.HTTP_400_BAD_REQUEST)

        event = get_or_none(Event, id=event_id)
        if event is None:
            return get_error_response(message='Event not found', server_status=status.HTTP_400_BAD_REQUEST)

        teams = Team.objects.filter(Event=event)
        team_race_stats_list = []
        for team in teams:
            race_detail = get_team_race_detail(team.id)
            team_race_stats_list.append({
                'team': TeamSerializer(team).data,
                'status': race_detail
            })
        return get_success_response(data=team_race_stats_list)

    def post(self, request, *args, **kwargs):
        event_id = request.data.get('event_id')
        name = request.data.get('name')
        #captain = request.data.get('captain')
        team_type = request.data.get('type')
        city = request.data.get('city')
        team_classification = request.data.get('classification')
        if name is None or team_type is None or team_classification is None or event_id is None or city is None:
            return get_error_response(message='Some parameters are missing', server_status=status.HTTP_400_BAD_REQUEST)

        event = get_or_none(Event, id=event_id)
        if event is None:
            return get_error_response(message='Event not found', server_status=status.HTTP_400_BAD_REQUEST)

        team = get_or_none(Team, Name=name, Event=event)
        if team is not None:
            return get_error_response(message='Team name already exists', server_status=status.HTTP_400_BAD_REQUEST)

        team_classification = team_classification.split(' ')[0]

        # Generate JoinCode
        generated = False
        while not generated:
            join_code = randint(100000, 999999)
            print(join_code)
            team = get_or_none(Team, JoinCode=join_code)
            if team is None:
                generated = True

        team_setting = TeamSetting.objects.create()
        # Add Team Record To Db
        try:
            new_team = Team.objects.create(Event=event, City=city, Setting=team_setting, JoinCode=str(join_code), StartTime='2020-02-03 10:10:00', Name=name, Type=team_type, Classification=team_classification)
            #new_team = Team.objects.create(Event=event, City=city, Setting=team_setting, JoinCode=str(join_code), StartTime='2020-02-03 10:10:00', Name=name, Captain=captain, Type=team_type, Classification=team_classification)
            race_detail = get_team_race_detail(new_team.id)
            # fb_write_team_status(new_team.Event.id, new_team.JoinCode, race_detail)
            return get_success_response(data=TeamSerializer(new_team).data)
        except:
            return get_error_response(message='Failed to create team', server_status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class ListCreateRunnerView(generics.ListCreateAPIView):
    """
    POST: Register Runner
    """
    permission_classes = (permissions.AllowAny,)
    
    @transaction.atomic
    def post(self, request, *args, **kwargs):
        team_id = request.data.get('TeamId')
        first_name = request.data.get('FirstName')
        last_name = request.data.get('LastName')
        email = request.data.get('Email')
        phone = request.data.get('Phone')
        city = request.data.get('City')
        state = request.data.get('State')
        zip_code = request.data.get('Zip')
        dob = request.data.get('Dob')
        shirt_size = request.data.get('ShirtSize')
        pace = request.data.get('Pace')
        gender = request.data.get('Gender')
        is_captain = request.data.get('IsCaptain')

        if team_id is None or first_name is None or last_name is None or email is None or phone is None or city is None or state is None or zip_code is None or dob is None or shirt_size is None or pace is None or gender is None or is_captain is None:
            return get_error_response('Some parameters are required', server_status=status.HTTP_400_BAD_REQUEST)
        
        team = get_or_none(Team, id=team_id)
        if team is None:
            return get_error_response(message='Team not found', server_status=status.HTTP_400_BAD_REQUEST)

        runner = get_or_none(Runner, Team=team, Email=email)
        if runner is not None:
            return get_error_response(message='Email has been already used', server_status=status.HTTP_400_BAD_REQUEST)

        runner_setting = RunnerNotificationSetting.objects.create()
        fcm_device = FCMDevice.objects.create(name="%s %s" % (first_name, last_name))

        try:
            new_runner = Runner.objects.create(
                Team=team,
                RunnerSetting=runner_setting,
                IsCaptain=is_captain,
                FirstName=first_name,
                LastName=last_name,
                Email=email,
                State=state,
                Phone= "+1 %s" % phone,
                City=city,
                FCMDevice=fcm_device,
                Zip=zip_code,
                DOB=datetime.datetime.strptime(dob, '%m/%d/%Y'),
                Pace=pace,
                Gender=gender,
                ShirtSize=shirt_size,
                SockSize='S-M'
            )
        except:
            return get_error_response(message='Failed to register runner', server_status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        
        if is_captain:
            send_email(email_address=email, message="""
            Thanks for registering to iRelay
            JoinCode for your team is %s
            """ % team.JoinCode)
        
        assign_runner_legs(team_id)
        
        runners = team.runner_set.filter(IsRoadie=0)
        # Award Register Badge
        register_badge = get_or_none(Badge, name='Register')
        new_runner.Badges.add(register_badge)
        if state != 'TX':
            award_badge(new_runner, 'Foreinger_Badge')
        
        return get_success_response(data=RunnerSerializer(runners, many=True).data)
            
    def get(self, request, *args, **kwargs):
        team_id = request.query_params.get('team_id')
        if team_id is None:
            return get_error_response(message='Team id is required', server_status=status.HTTP_400_BAD_REQUEST)

        team = get_or_none(Team, id=team_id)
        if team is None:
            return get_error_response(message='Team not found', server_status=status.HTTP_400_BAD_REQUEST)

        runners = team.runner_set.filter(IsRoadie=False)
        return get_success_response(data=RunnerSerializer(runners, many=True).data)


class RetrieveUpdateDestroyRunnerView(generics.RetrieveUpdateDestroyAPIView):
    """
    GET : Get Team Info And Team Runners Using JoinCode
    POSt : Register Team Roadie
    """
    permission_classes = (permissions.AllowAny,)

    # def get(self, request, *args, **kwargs):


class UpdateDeviceTokenView(generics.UpdateAPIView):
    """
    PUT: Update Runner FCM Notification
    """
    permission_classes = (permissions.AllowAny,)

    def put(self, request, *args, **kwargs):
        runner_id = request.data.get('runner_id')
        if runner_id is None:
            return get_error_response(message='Runner id is required', server_status=status.HTTP_400_BAD_REQUEST)

        runner = get_or_none(Runner, id=runner_id)
        if runner is None:
            return get_error_response(message='Runner not found', server_status=status.HTTP_400_BAD_REQUEST)

        fcm_token = request.data.get('fcm_token')
        if fcm_token is None:
            return get_error_response(message='FCM Token is required', server_status=status.HTTP_400_BAD_REQUEST)

        if request.data.get('type'):
            runner.FCMDevice.type = request.data.get('type')

        runner.FCMDevice.registration_id = fcm_token
        runner.FCMDevice.save()

        return get_success_response(data={})


# TeamInfo Fetch And Register New Roadie
class RaceLoginView(generics.ListCreateAPIView):
    """
    GET : Get Team Info And Team Runners Using JoinCode
    POSt : Register Team Roadie
    """
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):
        join_code = request.query_params.get('joinCode', None)
        if join_code is None:
            return get_error_response(message='Joincode is required', server_status=status.HTTP_400_BAD_REQUEST)
        try:
            app_admin = AppAdmin.objects.get(Code=join_code)
            return JsonResponse({
                'isSuccess': True,
                'admin': app_admin.AdminLevel
            })
        except:
            try:
                team = Team.objects.get(JoinCode=join_code)
                runners = team.runner_set.all().order_by('IsRoadie', 'FirstName')

                return JsonResponse({
                    'isSuccess': True,
                    'admin': 0,
                    'team': {
                        'id': team.id,
                        'name': team.Name,
                        'event_id': team.Event.id,
                        'course_id': team.Event.course.id,
                        'runners': RunnerSerializer(runners, many=True).data
                    }
                })
            except:
                return JsonResponse({
                    'isSuccess': False,
                    'error': 'Team not found.'
                })

    def post(self, request, *args, **kwargs):
        join_code = request.data.get('joinCode', None)
        name = request.data.get('name', None)
        if join_code is None or name is None:
            return Response(
                data={
                    'isSuccess': False,
                    'error': 'JoinCode And RoadieName Is Required'
                },
                status=status.HTTP_400_BAD_REQUEST
            )
        try:
            team = Team.objects.get(JoinCode=join_code)
            name_parts = name.split()
            first_name = name_parts[0]
            last_name = name_parts[1]
            Runner.objects.create(
                Team=team,
                FirstName=first_name,
                LastName=last_name,
                Pace=540,
                IsRoadie=1
            )
            new_roadie = Runner.objects.get(FirstName=first_name, LastName=last_name)
            return Response(
                data={
                    'isSuccess': True,
                    'runner': {
                        'key': new_roadie.id,
                        'isRoadie': 1,
                        'isCaptain': 0,
                        'name': "%s %s" % (first_name, last_name),
                        'pace': 540
                    }
                },
                status=status.HTTP_201_CREATED
            )
        except:
            return Response(
                data={
                    'isSuccess': False,
                    'error': 'Failed to register'
                },
                status=status.HTTP_404_NOT_FOUND
            )


class ListTeamStatusView(generics.ListAPIView):
    """
    GET : Get Team Race Info Including Completed Legs And RealTime Track Data
    """
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):

        team_id = request.query_params.get('teamId', None)
        race_detail = get_team_race_detail(team_id)

        if type(race_detail) is str:
            return get_error_response(message=race_detail, server_status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return get_success_response(data=race_detail)


class UpdateTeamStartTimeView(generics.UpdateAPIView):
    """
    PUT: Change Team Race Start Time
    """
    permission_classes = (permissions.AllowAny,)

    def put(self, request, *args, **kwargs):
        team_id = request.data.get('TeamId')
        str_start_time = request.data.get('StartTime')
        if team_id is None or str_start_time is None:
            return get_error_response(message='TeamId And StartTime Are Required', server_status=status.HTTP_400_BAD_REQUEST)

        team = get_or_none(Team, id=team_id)
        if team is None:
            return get_error_response(message='Team not found', server_status=status.HTTP_400_BAD_REQUEST)

        team.StartTime = str_start_time
        team.save()

        race_detail = get_team_race_detail(team_id)
        fb_write_team_status(team.Event.id, team.JoinCode, race_detail)
        return get_success_response(data=race_detail)


class UpdateSubmissionView(generics.UpdateAPIView):
    """
    PUT: Update Runner And Pace For Any Submission
    """
    permission_classes = (permissions.AllowAny,)

    def put(self, request, *args, **kwargs):

        runner_id = request.data.get('RunnerId')
        pace = request.data.get('Pace')
        team_id = request.data.get('TeamId')
        leg_num = request.data.get('LegNum')

        # Find Leg
        leg = get_or_none(Leg, Num=leg_num)
        if leg is None:
            return get_error_response(message='Leg not found', server_status=status.HTTP_400_BAD_REQUEST)

        # Find Team
        team = get_or_none(Team, id=team_id)
        if team is None:
            return get_error_response(message='Team not found', server_status=status.HTTP_400_BAD_REQUEST)

        # Find Runner
        try:
            runner = team.runner_set.get(id=runner_id)
        except:
            return get_error_response(message='Runner not found', server_status=status.HTTP_400_BAD_REQUEST)

        # Find Submission
        submission_to_update = get_or_none(Submission, Team=team, Leg=leg)
        if submission_to_update is None:
            return get_error_response(message='Submission not found', server_status=status.HTTP_400_BAD_REQUEST)

        # Update Submission
        submission_to_update.Pace = pace
        submission_to_update.Runner = runner
        submission_to_update.save()

        # Get Team Race Detail
        race_detail = get_team_race_detail(team_id)
        return get_success_response(data=race_detail)


class CreateRelayStartView(generics.CreateAPIView):
    """
    PUT: Update Runner And Pace For Any Submission
    """
    permission_classes = (permissions.AllowAny,)

    def post(self, request, *args, **kwargs):
        team_id = request.data.get('TeamId')
        if team_id is None:
            return get_error_response(message='TeamId is required', server_status=status.HTTP_400_BAD_REQUEST)
        team = get_or_none(Team, id=team_id)
        if team is None:
            return get_error_response(message='Team not found', server_status=status.HTTP_400_BAD_REQUEST)
    
        leg = Leg.objects.get(Num=1, Course=team.Event.course)
        first_submission = get_or_none(Submission, Team=team, Leg=leg)
        if first_submission is None:
            return get_error_response(message='There is no first submission', server_status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        # create team stats record
        # If team stats does exist, then delete that record
        try:
            TeamStats.objects.get(Team=team).delete()
        except:
            pass

        now_dt = datetime.datetime.now()

        # Find Leg Start Coordinate
        coordinate = leg.coordinates.all()[0]

        TeamStats.objects.create(
            Submission=first_submission,
            Team=team,
            TeamStartDt=now_dt,
            Coordinate=coordinate,
            DistanceOnVirtualCourse=0,
            LocationOnVirtualCourse=coordinate.Location,
            CurrentStartDt=now_dt,
        )

        first_submission.ReceivedDt = now_dt
        first_submission.save()
        
        # Award Start Badge
        award_badge(team, 'Start_Relay')
        
        race_detail = get_team_race_detail(team_id)
        # fb_write_team_status(team.Event.id, team.JoinCode, race_detail)
        if ALLOW_PUSH_NOTIFICATION:
            push_notification(topic='team_broadcast_%s' % team.JoinCode, title='Virtual Relay', description='Race has been started just now', data={
                'message': 'Race has been started just now',
                'status': 'start'
            })
        return get_success_response(data=race_detail)


class DestroyRelayStopView(generics.DestroyAPIView):
    """
    PUT: Update Runner And Pace For Any Submission
    """
    permission_classes = (permissions.AllowAny,)

    def delete(self, request, *args, **kwargs):
        team_id = request.query_params.get('TeamId')
        if team_id is None:
            return get_error_response(message='TeamId is required', server_status=status.HTTP_400_BAD_REQUEST)

        # Find Team
        team = get_or_none(Team, id=team_id)
        if team is None:
            return get_error_response(message='Team not found', server_status=status.HTTP_400_BAD_REQUEST)

        try:
            TeamStats.objects.get(Team=team)
        except:
            return get_error_response(message='Race has been already stopped', server_status=status.HTTP_400_BAD_REQUEST)
        
        TeamStats.objects.get(Team=team).delete()
        
        print('Deleted Team Stats')
        
        # Delete All Leg Tracks
        for team_submission in team.submission_set.all():
            team_submission.legtrack_set.all().delete()

        # IsActive = 0 for all submissions
        team.submission_set.update(IsFinished=0)

        race_detail = get_team_race_detail(team_id)
        # fb_write_team_status(team.Event.id, team.JoinCode, race_detail)

        if ALLOW_PUSH_NOTIFICATION:
            push_notification(topic='team_broadcast_%s' % team.JoinCode, title='Virtual Relay', description='Race has been stopped just now', data={
                'message': 'Race has been stopped just now',
                'status': 'stop'
            })
        return get_success_response(data=race_detail)


class ListCreateTrackView(generics.ListCreateAPIView):
    """
    POST : Create Track Record,
    GET : List Track Record
    """
    permission_classes = (permissions.AllowAny,)
    
    @transaction.atomic
    def post(self, request, *args, **kwargs):
        
        team_id = request.data.get('TeamId')
        leg_num = request.data.get('LegNum')
        location = request.data.get('Location')
        distance_delta = request.data.get('DeltaDistance')
        print("distance delta %f" % distance_delta)
        distance = request.data.get('Distance')
        speed = request.data.get('Speed')
        if team_id is None or leg_num is None or location is None or distance is None or speed is None:
            return get_error_response(message='Some parameters are required', server_status=status.HTTP_400_BAD_REQUEST)
        
        team = get_or_none(Team, id=team_id)
        if team is None:
            return get_error_response(message='Team not found', server_status=status.HTTP_400_BAD_REQUEST)

        leg = get_or_none(Leg, Num=leg_num, Course=team.Event.course)
        if leg is None:
            return get_error_response(message='Leg not found', server_status=status.HTTP_400_BAD_REQUEST)
        
        team_stats = get_or_none(TeamStats, Team=team)
        if team_stats is None:
            return get_error_response(message='Race has not been started yet', server_status=status.HTTP_400_BAD_REQUEST)
        
        if leg_num != team_stats.Submission.Leg.Num:
            return get_error_response(message='Leg %d has not been activated yet' % leg_num, server_status=status.HTTP_400_BAD_REQUEST)
        
        submission = get_or_none(Submission, Team=team, Leg=leg)
        if submission is None:
            return get_error_response(message='Submission not found', server_status=status.HTTP_400_BAD_REQUEST)
        
        start_dt = datetime.datetime.now()
        
        # Check Inner Point Where Is Between Two Coordinates And Delete That
        try:
            last_two_tracks = submission.legtrack_set.all().order_by('-id')[:3]
            end_coordinate = LatLng(float(location.split(",")[1]), float(location.split(",")[2]))
            start_lat = float(last_two_tracks[2].Location.split(',')[1])
            start_lng = float(last_two_tracks[2].Location.split(',')[2])
            start_coordinate = LatLng(start_lat, start_lng)
            target_lat = float(last_two_tracks[1].Location.split(',')[1])
            target_lng = float(last_two_tracks[1].Location.split(',')[2])
            target_coordinate = LatLng(target_lat, target_lng)
            if is_between_two_coordinates(start_coordinate=start_coordinate, end_coordinate=end_coordinate, target_coordinate=target_coordinate):
                last_two_tracks[1].delete()
        except:
            pass
        
        time_for_check_inline = datetime.datetime.now() - start_dt
        
        # Get Runner Location On Virtual Course
        future_coordinates = team_stats.Submission.Leg.coordinates.filter(id__gt=team_stats.Coordinate.id)
        
        start_dt = datetime.datetime.now()
        
        for current_coordinate in future_coordinates:
            
            real_distance = distance - team_stats.DistanceOnVirtualCourse
            prev_coordinate = team_stats.Coordinate
            
            p_lat = float(prev_coordinate.Location.split(",")[0])
            p_lon = float(prev_coordinate.Location.split(",")[1])
            
            n_lat = float(current_coordinate.Location.split(',')[0])
            n_lon = float(current_coordinate.Location.split(',')[1])
            
            distance_from_prev_coordinate = calculate_distance([p_lat, p_lon], [n_lat, n_lon])
            
            # print('realDistance: %f' % real_distance)
            # print('prevCoordinate: %f %f' % (p_lat, p_lon))
            # print('odometer: %f' % distance)
            # print('distance on virtualcourse: %f' % team_stats.DistanceOnVirtualCourse)
            # print('distance from prev coordinate: %f' %  distance_from_prev_coordinate)         
            
            if distance_from_prev_coordinate > real_distance:
                target_lat, target_lon = find_coordinate_between_tow_coordinates([p_lat, p_lon], [n_lat, n_lon], real_distance)
                team_stats.LocationOnVirtualCourse = "%s,%s" % (target_lat, target_lon)
                team_stats.save()
                break
            else:
                team_stats.Coordinate = current_coordinate
                team_stats.DistanceOnVirtualCourse = team_stats.DistanceOnVirtualCourse + distance_from_prev_coordinate
                team_stats.LocationOnVirtualCourse = "%s,%s" % (n_lat, n_lon)
                team_stats.save()

        # Save Overall Pace
        seconds_since_start = datetime.datetime.now() - team_stats.TeamStartDt
        overall_pace = seconds_since_start.seconds / distance
        team_stats.OverallPace = overall_pace
        team_stats.TotalMile = team_stats.TotalMile + distance_delta
        team_stats.UpdatedDt = datetime.datetime.now()
        team_stats.save()
        
        time_for_coordinate_calculation = datetime.datetime.now() - start_dt

        try:
            LegTrack.objects.create(Location=location, Distance=distance, Speed=speed, Submission=submission)
        except:
            return get_error_response(message='Failed to insert track record',
                                      server_status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        
        # Award Various Badge
        if team_stats.TotalMile > 94.61:
            # Award Half_Ran Badge
            award_badge(team, badge_name='Half_Ran')
        elif leg_num == 1 and team_stats.TotalMile > 0.5:
            award_badge(team, badge_name='Reached_Gonzales_Courthouse')
        elif leg_num == 3 and distance > 2.43:
            award_badge(team, badge_name='Passed_By_Oil_Wells')
        elif leg_num == 10 and distance > 2.16:
            award_badge(team, badge_name='Ran_Heighest_Elevation')
        elif leg_num ==12 and distance > 1:
            award_badge(team, badge_name='Ran_TexasY_Windmill')
        elif leg_num == 7 and distance > 2.5:
            award_badge(team, badge_name='Ran_Obscure_Historical_Marker_In_Texas')
        elif leg_num == 13 and distance > 4.5:
            award_badge(team, badge_name='Ran_Fainting_Goats')
        elif leg_num ==15 and distance > 5:
            award_badge(team, badge_name='Made_To_Iconic_Weimar_Water_Tower')
        elif leg_num == 21 and distance > 2.6:
            award_badge(team, badge_name='Ran_Colorado_River_Bridge')
        elif leg_num == 22 and distance > 1:
            award_badge(team, badge_name='Made_To_Bucee')
        elif leg_num == 31 and distance > 2.5:
            award_badge(team, badge_name='Made_To_Bush_Park')
        elif leg_num == 34 and distance > 3.4:
            award_badge(team, badge_name='Made_To_Rich_People_NeighborHood')
        elif leg_num == 35 and distance > 3:
            award_badge(team, badge_name='Made_To_Buffalo_Bayou_Park')
            
        race_detail = get_team_race_detail(team_id)
        return get_success_response(data=race_detail)

    def get(self, request, *args, **kwargs):
        team_id = request.query_params.get('TeamId')
        leg_num = request.query_params.get('LegNum')

        if team_id is None or leg_num is None:
            return get_error_response(message='TeamId and LegNum are required', server_status=status.HTTP_400_BAD_REQUEST)

        team = get_or_none(Team, id=team_id)
        if team is None:
            return get_error_response(message='Team not found', server_status=status.HTTP_400_BAD_REQUEST)
        
        leg = get_or_none(Leg, Num=leg_num, Course=team.Event.course)
        if leg is None:
            return get_error_response(message='Leg not found', server_status=status.HTTP_400_BAD_REQUEST)

        submission = get_or_none(Submission, Team=team, Leg=leg)
        if submission is None:
            return get_error_response(message='Submission not found', server_status=status.HTTP_400_BAD_REQUEST)

        # Fetch Track Records
        track_records = submission.legtrack_set.all()
        tracks = []
        for track in track_records:
            tracks.append({
                'Location': track.Location,
                'Distance': float(track.Distance),
                'Speed': float(track.Speed)
            })

        return get_success_response(data=tracks)


class UpdateHandOffView(generics.UpdateAPIView):
    """
    PUT : Save Last Track Record And Start Next Leg And Inform Team Members
    """
    permission_classes = (permissions.AllowAny,)
    
    def put(self, request, *args, **kwargs):
        
        team_id = request.data.get('TeamId')
        leg_num = request.data.get('LegNum')
        location = request.data.get('Location')
        distance = request.data.get('Distance')
        speed = request.data.get('Speed')
        distance_delta = request.data.get('DeltaDistance')
        
        if team_id is None or leg_num is None or location is None or distance is None or speed is None:
            return get_error_response(message='Some parameters are required', server_status=status.HTTP_400_BAD_REQUEST)

        # Find Team
        team = get_or_none(Team, id=team_id)
        if team is None:
            return get_error_response(message='Team not found', server_status=status.HTTP_400_BAD_REQUEST)

        # Find Leg
        leg = get_or_none(Leg, Num=leg_num, Course=team.Event.course)
        if leg is None:
            return get_error_response(message='Leg not found', server_status=status.HTTP_400_BAD_REQUEST)

        team_stats = get_or_none(TeamStats, Team=team)
        if team_stats is None:
            return get_error_response(message='Race has not been started yet',
                                      server_status=status.HTTP_400_BAD_REQUEST)

        if leg_num != team_stats.Submission.Leg.Num:
            return get_error_response(message='Leg %d has not been activated yet',
                                      server_status=status.HTTP_400_BAD_REQUEST)

        # Find Submission
        submission = get_or_none(Submission, Team=team, Leg=leg)
        if submission is None:
            return get_error_response(message='Submission not found', server_status=status.HTTP_400_BAD_REQUEST)

        # Check Inner Point Where Is Between Two Coordinates And Delete That
        try:
            last_two_tracks = submission.legtrack_set.all().order_by('-id')[:2]
            end_coordinate = LatLng(float(location.split(",")[0]), float(location.split(",")[1]))
            start_lat = float(last_two_tracks[1].Location.split(',')[0])
            start_lng = float(last_two_tracks[1].Location.split(',')[1])
            start_coordinate = LatLng(start_lat, start_lng)
            target_lat = float(last_two_tracks[0].Location.split(',')[0])
            target_lng = float(last_two_tracks[0].Location.split(',')[1])
            target_coordinate = LatLng(target_lat, target_lng)
            print(target_coordinate)
            if is_between_two_coordinates(start_coordinate=start_coordinate, end_coordinate=end_coordinate,
                                          target_coordinate=target_coordinate):
                last_two_tracks[0].delete()
                print('Has been deleted')
        except:
            pass
        
        # Create Track Record
        try:
            LegTrack.objects.create(Location=location, Distance=distance, Speed=speed, Submission=submission)
        except:
            return get_error_response(message='Failed to insert track record',
                                      server_status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        # Update Team Status
        now_dt = datetime.datetime.now()
        submission.ExchangeDt = now_dt
        submission.IsFinished = 1
        submission.save()

        team_stats = TeamStats.objects.get(Team=team)

        if leg_num is not 36:
            next_leg = Leg.objects.get(Num=leg_num + 1, Course=team.Event.course)
            next_submission = Submission.objects.get(Leg=next_leg, Team=team)
            next_submission.ReceivedDt = now_dt
            next_submission.save()
            team_stats.CurrentStartDt = now_dt
            team_stats.Submission = next_submission
            
            # Find Leg Start Coordinate
            coordinate = next_leg.coordinates.all()[0]
            team_stats.Coordinate = coordinate
            team_stats.DistanceOnVirtualCourse = 0
            team_stats.LocationOnVirtualCourse = coordinate.Location
            description = 'Leg %d has been finished' % leg_num
            
            # Award Leg Finish Badge
            runner = submission.Runner
            badge_name = 'Leg%d_Ran' % leg_num
            award_badge(runner, badge_name)
            if leg_num == 2:
                award_badge(team, 'Made_To_Sam_Houston_Oak')
            elif leg_num ==3:
                award_badge(team, 'Ran_Virtual_Dirt_Road')
            elif leg_num == 5:
                award_badge(runner, 'Reached_Shiner')
            elif leg_num == 5:
                award_badge(team, 'Saw_Texas_WildFlowers')
            elif leg_num == 8:
                award_badge(team, 'Saw_Cows_Bulls')
            elif leg_num == 10:
                award_badge(runner, 'Reached_Flatonia')
            elif leg_num == 11:
                award_badge(runner, 'Reached_Praha')
            elif leg_num == 13:
                award_badge(runner, 'Reached_Schulenburg')
            elif leg_num == 15:
                award_badge(runner, 'Reached_Weimar')
            elif leg_num == 18:
                award_badge(runner, 'Reached_Columbus')
                award_badge(team, 'Made_To_Snappy')
            elif leg_num == 19:
                award_badge(team, 'Made_To_Columbus_Airport')
            elif leg_num == 20:
                award_badge(team, 'Made_To_Lit_Silo')                
            elif leg_num == 21:
                award_badge(runner, 'Reached_Eagle_Lake')
            elif leg_num == 23:
                award_badge(team, 'Made_To_Giant_Grain_Elevator')                
            elif leg_num == 25:
                award_badge(runner, 'Reached_Wallis')
                award_badge(team, 'Made_To_Brazo_High_School')    
            elif leg_num == 26:
                award_badge(team, 'Made_To_Brazo_River')    
            elif leg_num == 27: 
                award_badge(runner, 'Reached_Simonton')
            elif leg_num == 28: 
                award_badge(runner, 'Reached_Fulshear')
            elif leg_num == 29: 
                award_badge(runner, 'Reached_Cinco_Ranch')
            elif leg_num == 30:
                award_badge(team, 'Made_To_Good_Times_Running_Co')                    
            elif leg_num == 31: 
                award_badge(runner, 'Reached_Houston')
            elif leg_num == 32:
                award_badge(runner, 'Made_To_Hershey_Trail')
            elif leg_num == 35:
                award_badge(runner, 'Made_To_Memorial_Park')
        else:
            team_stats.IsFinished = True
            award_badge(runner, 'Finished_At_Waterworks')
            description = 'Race has been finished'
        # Save Overall Pace
        seconds_since_start = datetime.datetime.now() - team_stats.TeamStartDt
        overall_pace = seconds_since_start.seconds / distance
        team_stats.OverallPace = overall_pace
        team_stats.TotalMile = team_stats.TotalMile + distance_delta
        team_stats.UpdatedDt = datetime.datetime.now()
        team_stats.save()

        race_detail = get_team_race_detail(team_id)
        if ALLOW_PUSH_NOTIFICATION:
            push_notification(topic='team_broadcast_%s' % team.JoinCode, title='Virtual Relay', description=description, data={
                'message': description,
                'status': 'handoff'
            })
        
        # Send sms to next runner
        if not TEST:
            phone_no = next_submission.Runner.phone
            try:
                send_sms(sender_phone=phone_no, message='iRelay\nLeg %d has been started. Your turn to run!' % next_submission.Leg.Num);
            except:
                pass
        return get_success_response(data=race_detail)


class ListCreateMediaView(generics.ListCreateAPIView):
    """
    GET: List All Media Of Any Team
    POST: Upload Team Image
    """
    permission_classes = (permissions.AllowAny,)
    serializer_class = MediaSerializer
    
    def get(self, request, *args, **kwargs):
        team_id = request.query_params.get('team_id')
        if team_id is None:
            return get_error_response(message='TeamId is required', server_status=status.HTTP_400_BAD_REQUEST)
        
        team = get_or_none(Team, id=team_id)
        if team is None:
            return get_error_response(message='Team not found', server_status=status.HTTP_400_BAD_REQUEST)

        media = Media.objects.filter(team=team).order_by('-created')
        media_serializer = MediaSerializer(media, many=True)
        return get_success_response(data=media_serializer.data)
    
    def post(self, request, *args, **kwargs):
        runner_id = request.data.get('RunnerId')
        thumb_image = request.FILES.get('Thumb')
        if runner_id is None:
            return get_error_response(message='Runner id is required', server_status=status.HTTP_400_BAD_REQUEST)
        
        runner = get_or_none(Runner, id=runner_id)
        if runner is None:
            return get_error_response(message='Runner not found', server_status=status.HTTP_400_BAD_REQUEST)

        media_type = request.data.get('Type')
        if media_type is None:
            return get_error_response(message='Media type is required', server_status=status.HTTP_400_BAD_REQUEST)

        if media_type != 'Image' and media_type != 'Video':
            return get_error_response(message='Supported file formats are Image and Video', server_status=status.HTTP_400_BAD_REQUEST)

        if media_type == 'Video' and not thumb_image:
            return get_error_response(message='Thumb image is required for video', server_status=status.HTTP_400_BAD_REQUEST)

        if request.FILES.get('File') is None:
            return get_error_response(message='File is required', server_status=status.HTTP_400_BAD_REQUEST)

        try:
            new_medium = Media.objects.create(runner=runner, team=runner.Team, media_type=media_type, thumb=thumb_image, content=request.FILES.get('File'))
            media_serializer = MediaSerializer(new_medium)

            # # Send Push Notification To Team Members
            runners = runner.Team.runner_set.exclude(id=runner_id)
            message = "New media has been uploaded by %s %s" % (runner.FirstName, runner.LastName)
            
            # Award Upload Badge
            try:
                upload_badge = runner.Badges.get(name='Media_Upload')
            except:
                upload_badge = Badge.objects.get(name='Media_Upload')
                runner.Badges.add(upload_badge)
            
            for runner in runners:
                if runner.RunnerSetting.whenContributionToTeamPage:
                    if not TEST:
                        runner.FCMDevice.send_message(title="iRelay", body=message, data={
                            'message': message
                        })
            
            return get_success_response(data=media_serializer.data)
        except:
            return get_error_response(message='Failed to create media record', server_status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class ListCreateMessageView(generics.ListCreateAPIView):
    """
    GET: List All Messages Of Any Team
    POST: Upload Message
    """
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):
        team_id = request.query_params.get('team_id')
        if team_id is None:
            return get_error_response(message='Team Id is required', server_status=status.HTTP_400_BAD_REQUEST)

        team = get_or_none(Team, id=team_id)
        if team is None:
            return get_error_response(message='Team not found', server_status=status.HTTP_400_BAD_REQUEST)

        messages = Message.objects.filter(team=team).order_by('-created')
        return get_success_response(data=MessageSerializer(messages, many=True).data)

    def post(self, request, *args, **kwargs):
        runner_id = request.data.get('runner_id')
        if runner_id is None:
            return get_error_response(message='Runner id is required', server_status=status.HTTP_400_BAD_REQUEST)

        runner = get_or_none(Runner, id=runner_id)
        if runner is None:
            return get_error_response(message='Runner not found', server_status=status.HTTP_400_BAD_REQUEST)

        message = request.data.get('message')
        if message is None:
            return get_error_response(message='Message is required', server_status=status.HTTP_400_BAD_REQUEST)

        try:
            new_message = Message.objects.create(message=message, runner=runner, team=runner.Team)
             # # Send Push Notification To Team Members
            runners = runner.Team.runner_set.exclude(id=runner_id)
            message = "New message arrived from %s %s" % (runner.FirstName, runner.LastName)
            if not TEST:
                for runner in runners:
                    runner.FCMDevice.send_message(title="iRelay", body=message, data={
                        'message': message
                    })
            return get_success_response(data=MessageSerializer(new_message).data)
        except:
            return get_error_response(message='Failed to create message record', server_status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class RetrieveUpdateSettingView(generics.ListCreateAPIView):
    """
    GET : Fetch Team Setting And Runner Setting
    POST : Update Team Setting And Runner Setting
    """
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):
        runner_id = request.query_params.get('runner_id')
        if runner_id is None:
            return get_error_response(message='Runner Id is required', server_status=status.HTTP_400_BAD_REQUEST)

        runner = get_or_none(Runner, id=runner_id)
        if runner is None:
            return get_error_response(message='Runner not found', server_status=status.HTTP_400_BAD_REQUEST)

        runner_setting = runner.RunnerSetting
        team_setting = runner.Team.Setting

        return get_success_response(data={
            'team_photo': runner.Team.Logo.url,
            'runner_photo': runner.Photo.url,
            'team_setting': TeamSettingSerializer(team_setting).data,
            'runner_setting': RunnerNotificationSettingSerializer(runner_setting).data
        })

    def post(self, request, *args, **kwargs):
        runner_id = request.data.get('runner_id')
        if runner_id is None:
            return get_error_response(message='Runner id is required', server_status=status.HTTP_200_OK)

        runner = get_or_none(Runner, id=runner_id)
        if runner is None:
            return get_error_response(message='Runner not found', server_status=status.HTTP_200_OK)

        if request.FILES.get('runner_photo'):
            runner.Photo = request.FILES['runner_photo']
            runner.save()

        if request.FILES.get('team_photo'):
            runner.Team.Logo = request.FILES['team_photo']
            runner.Team.save()

        team_setting_data = request.data.get('team_setting')
        team_setting_data = json.loads(team_setting_data)

        runner_setting_data = request.data.get('runner_setting')
        runner_setting_data = json.loads(runner_setting_data)

        team_setting_serializer = TeamSettingSerializer(data=team_setting_data, instance=runner.Team.Setting)
        runner_setting_serializer = RunnerNotificationSettingSerializer(data=runner_setting_data, instance=runner.RunnerSetting)

        team_setting_serializer.is_valid()
        team_setting_serializer.save()

        runner_setting_serializer.is_valid()
        runner_setting_serializer.save()

        return get_success_response(data={
            'message': 'success'
        })


# Fetch Finished Legs For Any Team
class ListFinishedLegsView(generics.ListAPIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):
        team_id = request.query_params.get('team_id')
        if team_id is None:
            return get_error_response(message='Team id is required', server_status=status.HTTP_400_BAD_REQUEST)
        
        team = get_or_none(Team, id=team_id)
        if team is None:
            return get_error_response(message='Team not found', server_status=status.HTTP_400_BAD_REQUEST)

        team_stats = get_or_none(TeamStats, Team=team)
        if team_stats is None:
            return get_success_response(data=[])

        finished_legs = []
        completed_submissions = team.submission_set.filter(IsFinished=1)
        for submission in completed_submissions:
            leg_time_diff = submission.ExchangeDt - submission.ReceivedDt
            total_time_diff = submission.ExchangeDt - team_stats.TeamStartDt
            
            finished_legs.append({
                'LegNum': submission.Leg.Num,
                'RunnerName': submission.Runner.FirstName + " " + submission.Runner.LastName,
                'RunnerPhoto': submission.Runner.Photo.url,
                'RunnerId': submission.Runner.id,
                'TotalMiles': float(submission.Leg.TotalMiles),
                'Miles': float(submission.Leg.Miles),
                'Time': leg_time_diff.seconds,
                'TotalTime': total_time_diff.seconds,
                'Pace': leg_time_diff.seconds / submission.Leg.Miles,
                'TotalPace': total_time_diff.seconds / submission.Leg.TotalMiles,
                'StartTs': get_timestamp(submission.ReceivedDt),
                'FinishedTs': get_timestamp(submission.ExchangeDt)
            })

        return get_success_response(data=finished_legs)


class ListFutureSubmissionsView(generics.ListAPIView):
    """
    GET : List Future Submissions
    """
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):
        team_id = request.query_params.get('team_id')
        if team_id is None:
            return get_error_response(message='Team id is required', server_status=status.HTTP_400_BAD_REQUEST)

        team = get_or_none(Team, id=team_id)
        if team is None:
            return get_error_response(message='Team not found', server_status=status.HTTP_400_BAD_REQUEST)
    
        runners = team.runner_set.all()

        # Future Submissions Not Yet Done
        future_submissions = team.submission_set.filter(IsFinished=0)
        future_submission_list = []
        for submission in future_submissions:
            future_submission_list.append({
                'Leg': submission.Leg.Num,
                'Miles': submission.Leg.Miles,
                'TotalMiles': submission.Leg.TotalMiles,
                'RunnerName': submission.Runner.FirstName + " " + submission.Runner.LastName,
                'Pace': submission.Pace,
                'RunnerId': submission.Runner.id
            })

        return get_success_response(data={
            'submissions': future_submission_list,
            'runners': RunnerSerializer(runners, many=True).data
        })


class ListResultView(generics.ListAPIView):
    permission_classes = (permissions.AllowAny,)
    
    def get(self, request, *args, **kwargs):
        event_id = request.query_params.get('event_id')
        if event_id is None:
            return get_error_response(message='Event id is required', server_status=status.HTTP_400_BAD_REQUEST)
        
        event = get_or_none(Event, id=event_id)
        
        if event is None:
            return get_error_response(message='Event not found', server_status=status.HTTP_400_BAD_REQUEST)
        
        teams = event.team_set.all()
        
        result_list = []
        for team in teams:
            team_stats = get_or_none(TeamStats, Team=team)
            if team_stats is None:
                continue
            if not team_stats.IsFinished:
                continue
            time_diff = team_stats.UpdatedDt - team_stats.TeamStartDt
            overall_seconds = time_diff.seconds
            total_pace = 0
            if team_stats.TotalMile != 0:
                total_pace = int(overall_seconds / team_stats.TotalMile)
            result_list.append({
                'id': team.id,
                'type': team.Type,
                'classification': team.Classification,
                'name': team.Name,
                'is_finished': team_stats.IsFinished,
                'total_time': overall_seconds,
                'total_mile': team_stats.TotalMile,
                'total_pace': total_pace
            })
        return get_success_response(data=result_list)


class ListTeamSplitOverviewView(generics.ListAPIView):
    permission_classes = (permissions.AllowAny,)
    
    def get(self, request, *args, **kwargs):
        event_id = request.query_params.get('event_id')
        if event_id is None:
            return get_error_response(message='Event id is required', server_status=status.HTTP_400_BAD_REQUEST)
        
        event = get_or_none(Event, id=event_id)
        
        if event is None:
            return get_error_response(message='Event not found', server_status=status.HTTP_400_BAD_REQUEST)
        
        teams = event.team_set.all()
        
        split_overview_list = []
        
        for team in teams:
            team_stats = get_or_none(TeamStats, Team=team, IsFinished=1)
            if team_stats is None:
                continue
            split_overview_list.append({
                'id': team.id,
                'name': team.Name,
                'start_time': get_timestamp(team_stats.TeamStartDt),
                'finish_time': get_timestamp(team_stats.UpdatedDt),
                'distance': team_stats.TotalMile,
            })
        return get_success_response(data=split_overview_list)


class ListTeamSplitDetailView(generics.ListAPIView):
    permission_classes = (permissions.AllowAny,)
    
    def get(self, request, *args, **kwargs):
        event_id = request.query_params.get('team_id')
        if event_id is None:
            return get_error_response(message='Team id is required', server_status=status.HTTP_400_BAD_REQUEST)
        
        team = get_or_none(Team, id=event_id)
        
        if team is None:
            return get_error_response(message='Team not found', server_status=status.HTTP_400_BAD_REQUEST)
        
        submissions = team.submission_set.all()

        split_overview_list = []
        
        for submission in submissions:
            split_overview_list.append({
                'id': submission.Leg.Num,
                'name': 'Leg %d' % submission.Leg.Num,
                'start_time': get_timestamp(submission.ReceivedDt),
                'finish_time': get_timestamp(submission.ExchangeDt),
                'distance': submission.Leg.Miles,
            })
        return get_success_response(data=split_overview_list)


class ListPublishView(generics.ListAPIView):
    permission_classes = (permissions.AllowAny,)
    
    def get(self, request, *args, **kwargs):
        json_data = {'message': 'Hello to all connected clients', 'date': '2019-02-02', 'title': 'welcome'}
        publish_data_on_redis(json_data, 'notification.new')    
        return get_success_response(data={
           'status': 'sent' 
        });
      
      
class CalculateDistance(generics.ListAPIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):
        lat1 = float(request.query_params.get('lat1'))
        lon1 = float(request.query_params.get('lon1'))
        lat2 = float(request.query_params.get('lat2'))
        lon2 = float(request.query_params.get('lon2'))

        return get_success_response(data={
            # 'distance': distance
            'distance': calculate_distance([lat1, lon1], [lat2, lon2])
            # 'distance': Haversine([lat1, lon1], [lat2, lon2]).miles
        })


class CreateSendSmsView(generics.CreateAPIView):
    permission_classes = (permissions.AllowAny,)
    
    def post(self, request, *args, **kwargs):
        
        phone = request.data.get('phone')
        message = request.data.get('message')
        if phone is None or message is None:
            return get_error_response(message='Phone number and message are required', server_status=status.HTTP_400_BAD_REQUEST)
        
        try:
            send_sms(sender_phone=phone, message=message)
            return get_success_response(data={
                'status': 'sent'
            })
        except:
            return get_error_response(message='Failed to send sms', server_status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class CreateSendemailView(generics.CreateAPIView):
    permission_classes = (permissions.AllowAny,)
    
    def post(self, request, *args, **kwargs):
        address = request.data.get('email_address')
        content = request.data.get('message')
        if address is None or content is None:
            return get_error_response(message='Email address or message are required', server_status=status.HTTP_400_BAD_REQUEST)
        
        send_email(email_address=address, message=content)
        return get_success_response(data={
            'address': address,
            'content': content
        })