import os
from django.core.exceptions import ValidationError


def validate_image_file_extension(value):
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.png', '.bmp', '.jpg', '.jpeg', '.gif', '.tif', '.tiff']
    if not ext.lower() in valid_extensions:
        raise ValidationError('Unsupported file extension.')


def validate_video_file_extension(value):
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.mov', '.avi', '.mpeg', '.MOV', '.AVI']
    if not ext.lower() in valid_extensions:
        raise ValidationError('Unsupported file extension.')
